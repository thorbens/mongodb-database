# Description

This repository provides a mongodb based implementation for [@thorbens/database]([https://gitlab.com/thorbens/database).

Node >= 14 is required.
