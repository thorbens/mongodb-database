import {
  isMongoDatabaseOptionsProvider,
  MongoDatabaseCollectionOptions,
  MongoDatabaseOptionsProvider,
} from "../mapping";
import { FieldOptions, RepositoryMapping } from "./decorators";

// tslint:disable-next-line:max-classes-per-file
@RepositoryMapping({ collectionName: "bar" })
class WithoutFieldMapping {}

// tslint:disable-next-line:max-classes-per-file
@RepositoryMapping()
class WithFieldMapping {
  @FieldOptions({ createIndex: true })
  public bar = "";
  public foo = "";
}

// tslint:disable-next-line:max-classes-per-file
@RepositoryMapping()
class WithMultipleFieldMapping {
  @FieldOptions({ createIndex: true })
  public bar = "";
  @FieldOptions({ createIndex: true, indexType: "text" })
  public foo = "";
}

describe("RepositoryMapping", () => {
  it("should return the correct options", async () => {
    const instance = new WithoutFieldMapping();
    expect(isMongoDatabaseOptionsProvider(instance)).toEqual(true);
    const options = (instance as MongoDatabaseOptionsProvider).getCollectionOptions();
    expect(options).toEqual({
      collectionName: "bar",
      properties: {},
    } as MongoDatabaseCollectionOptions);
  });

  it("should return the correct options if a field option is set", async () => {
    const instance = new WithFieldMapping();
    expect(isMongoDatabaseOptionsProvider(instance)).toEqual(true);
    const options = (instance as any).getCollectionOptions();
    expect(options).toEqual({
      collectionName: "WithFieldMapping",
      properties: {
        bar: {
          createIndex: true,
        },
      },
    } as MongoDatabaseCollectionOptions);
  });

  it("should return the correct options if multiple field options are set", async () => {
    const instance = new WithMultipleFieldMapping();
    expect(isMongoDatabaseOptionsProvider(instance)).toEqual(true);
    const options = (instance as any).getCollectionOptions();
    expect(options).toEqual({
      collectionName: "WithMultipleFieldMapping",
      properties: {
        bar: {
          createIndex: true,
        },
        foo: {
          createIndex: true,
          indexType: "text",
        },
      },
    } as MongoDatabaseCollectionOptions);
  });
});
