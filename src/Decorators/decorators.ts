import "reflect-metadata";
import {
  MongoDatabaseCollectionOptions,
  MongoDatabaseFieldOptions,
  MongoDatabaseOptionsProvider,
} from "../mapping";

const META_DATA_KEY = "mongodb:fieldmappings";

export function RepositoryMapping(
  collectionOptions: MongoDatabaseCollectionOptions = {}
) {
  collectionOptions = {
    ...collectionOptions,
  };

  return <T extends new (...args: any[]) => {}>(constructor: T) => {
    if (!collectionOptions.collectionName) {
      // set the name if no name is given as the constructor name is only available in this class
      // after extending and compilation, the a different class name will be assigned (e.g. "class_1")
      collectionOptions.collectionName = constructor.name;
    }

    return class extends constructor implements MongoDatabaseOptionsProvider {
      public getCollectionOptions(): MongoDatabaseCollectionOptions {
        return {
          ...collectionOptions,
          properties: Object.keys(this).reduce((collector, key) => {
            const fieldMapping = Reflect.getMetadata(META_DATA_KEY, this, key);
            if (!fieldMapping) {
              return collector;
            }

            return {
              ...collector,
              [key]: fieldMapping,
            };
          }, {}),
        };
      }
    };
  };
}

export function FieldOptions(fieldMapping: MongoDatabaseFieldOptions) {
  return Reflect.metadata(META_DATA_KEY, fieldMapping);
}
