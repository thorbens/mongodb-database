/**
 * Represents an mongo db id provider.
 */
export interface MongoDatabaseIDProvider {
  /**
   * The mongo db id for this id provider.
   */
  readonly _id: string;
}
