import { DatabaseModel } from "@thorbens/database";
import { FieldOptions, RepositoryMapping } from "./Decorators";
import { MongoDatabase } from "./MongoDatabase";

const databaseOptions = {
  databaseName: process.env.MONGO_DB_DATABASE_NAME || "test",
  password: process.env.MONGO_DB_PASSWORD || "test",
  url: process.env.MONGO_DB_HOST || "mongodb://localhost:27017",
  username: process.env.MONGO_DB_USERNAME || "root",
};

// tslint:disable-next-line:max-classes-per-file
@RepositoryMapping()
class MultipleFieldMappingDatabaseModel implements DatabaseModel {
  public id!: string;
  @FieldOptions({ createIndex: true })
  public bar = "";
  @FieldOptions({ createIndex: true })
  public foo = "";
}

describe("MongoDatabase", () => {
  it("should be disconnected on construct", async () => {
    const database = new MongoDatabase(databaseOptions);
    expect(database.isConnected()).toEqual(false);
  });

  it("should disconnect correctly", async () => {
    const database = new MongoDatabase(databaseOptions);
    await database.connect();
    expect(database.isConnected()).toEqual(true);
    await database.disconnect();
    expect(database.isConnected()).toEqual(false);
  });

  it("should create an index correctly for the given database model", async () => {
    const database = new MongoDatabase(databaseOptions);
    // drop before creating
    try {
      await database.dropRepository(MultipleFieldMappingDatabaseModel);
      const repository = await database.getRepository(
        MultipleFieldMappingDatabaseModel
      );
      expect(repository).toBeDefined();
      expect(await repository.getName()).toEqual(
        "MultipleFieldMappingDatabaseModel"
      );
    } finally {
      await database.disconnect();
    }
  });
});
