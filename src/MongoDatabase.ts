import {
  CommonDatabaseQuery,
  Database,
  DatabaseConditionBuilder,
  DatabaseConditionParser,
  DatabaseModel,
  DatabaseOptions,
  DatabaseQuery,
} from "@thorbens/database";
import { Collection, Db, MongoClient, MongoClientOptions } from "mongodb";
import { isMongoDatabaseOptionsProvider } from "./mapping";
import { MongoDatabaseConditionBuilder } from "./MongoDatabaseConditionBuilder";
import { MongoDatabaseConditionParser } from "./MongoDatabaseConditionParser";
import { MongoRepository } from "./MongoRepository";

type RepositoryConstructor<T extends DatabaseModel> = new (...args: any[]) => T;

/**
 * Database implementation for a mongo database.
 */
export class MongoDatabase implements Database {
  /**
   * The mongo database client.
   */
  private client!: MongoClient;
  /**
   * The database used.
   */
  private database!: Db;
  /**
   * Indicator to check if there is a connection to the database or not.
   */
  private connected = false;
  /**
   * If a connection is currently established.
   */
  private connecting = false;
  /**
   * Singleton condition builder.
   */
  private readonly conditionBuilder: MongoDatabaseConditionBuilder;
  /**
   * Singleton condition parser.
   */
  private readonly conditionParser: MongoDatabaseConditionParser;
  /**
   * The config for this database.
   */
  private readonly config: DatabaseOptions;
  /**
   * A map which contains already created repositories.
   * The key maps the repository name to a repository.
   */
  private readonly repositoryMap: Map<string, MongoRepository<any>> = new Map();

  public constructor(databaseOptions: DatabaseOptions) {
    this.config = databaseOptions;
    this.conditionBuilder = new MongoDatabaseConditionBuilder();
    this.conditionParser = new MongoDatabaseConditionParser(
      this.conditionBuilder
    );
  }

  public async getRepository<T extends DatabaseModel>(
    type: RepositoryConstructor<T>
  ): Promise<MongoRepository<T>> {
    // create new repository
    const instance = new type();
    let options;
    if (isMongoDatabaseOptionsProvider(instance)) {
      options = instance.getCollectionOptions();
    }
    const className = this.getClassNameOrThrow(type);
    await this.connect();

    if (this.repositoryMap.has(className)) {
      // can not be null
      return this.repositoryMap.get(className) as MongoRepository<T>;
    }
    const collection = await this.getOrCreateCollection<T>(className);
    const repository = new MongoRepository<T>(collection, options);
    await repository.updateMappings();
    this.repositoryMap.set(className, repository);

    return repository;
  }

  public async dropRepository<T extends DatabaseModel>(
    type: RepositoryConstructor<T>
  ): Promise<void> {
    const className = this.getClassNameOrThrow(type);
    const repository =
      this.repositoryMap.get(className) ?? (await this.getRepository(type));
    await repository?.drop();
    this.repositoryMap.delete(className);
  }

  public async connect(): Promise<void> {
    if (this.isConnected()) {
      return;
    }
    if (this.connecting) {
      // prevent multiple connections
      while (this.connecting) {
        await this.sleep(100);
      }
    } else {
      this.connecting = true;
      const options = {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      } as MongoClientOptions;
      // check if credentials are provided
      if (this.config.username && this.config.password) {
        options.auth = {
          password: this.config.password,
          username: this.config.username,
        };
      }
      this.client = await MongoClient.connect(this.config.url, options);
      // ser connected flag
      this.connected = true;
      // select the database
      this.database = this.client.db(this.config.databaseName);
      // connecting done, set state to false
      this.connecting = false;
    }
  }

  public async disconnect(): Promise<void> {
    if (this.connected) {
      await this.client.close();
      this.connected = false;
    }
  }

  /**
   * Returns true if a connection exists.
   */
  public isConnected(): boolean {
    return this.connected;
  }

  public createQuery<T>(): DatabaseQuery<T> {
    return new CommonDatabaseQuery<T>();
  }

  public getConditionBuilder<TModel = {}>(): DatabaseConditionBuilder<TModel> {
    return this.conditionBuilder;
  }

  public getConditionParser<TModel = {}>(): DatabaseConditionParser<TModel> {
    return this.conditionParser;
  }

  /**
   * Checks if a collection with the given name already exists and returns it.
   * If no collection exists, a new one is created.
   *
   * @param collectionName The collection name.
   */
  private async getOrCreateCollection<T>(
    collectionName: string
  ): Promise<Collection<T>> {
    const listCollectionsCursor = this.database.listCollections({
      name: collectionName,
    });
    const collections = await listCollectionsCursor.toArray();
    if (collections.length > 0) {
      // return the first collection as the collection name is unique
      return this.database.collection(collectionName);
    }

    return this.database.createCollection<T>(collectionName);
  }

  private getClassNameOrThrow<T extends DatabaseModel>(
    type: RepositoryConstructor<T>
  ): string {
    const instance = new type();
    let className = type.name;
    if (isMongoDatabaseOptionsProvider(instance)) {
      className = instance.getCollectionOptions()?.collectionName || className;
    }
    if (!className) {
      throw new Error(`could not get repository name from model class`);
    }

    return className;
  }

  /**
   * Sleeps for the given amount of milliseconds.
   *
   * @param milliseconds The amount of milliseconds to sleep.
   */
  private sleep(milliseconds: number): Promise<void> {
    return new Promise((resolve) => {
      setTimeout(resolve, milliseconds);
    });
  }
}
