import { MongoDatabaseConditionBuilder } from "./MongoDatabaseConditionBuilder";
import {
  EqualDatabaseOperator,
  MatchSomeDatabaseOperator,
  RegularExpresionDatabaseOperator,
} from "./Operators";

interface TestModel {
  foo: string;
}

describe("MongoDatabaseConditionBuilder", () => {
  it("should create the correct atom operators", async () => {
    const operatorBuilder = new MongoDatabaseConditionBuilder<TestModel>();
    const testKey = `foo`;
    const testValue = `bar`;
    const testRegExp = /foo/;

    const eqOperator = operatorBuilder.createEqualOperator(testKey, testValue);
    expect(eqOperator instanceof EqualDatabaseOperator).toEqual(true);
    expect(eqOperator.getKey()).toEqual(testKey);
    expect(eqOperator.getValue()).toEqual(testValue);

    const matchOperator = operatorBuilder.createMatchSomeOperator(
      testKey,
      testValue
    );
    expect(matchOperator instanceof MatchSomeDatabaseOperator).toEqual(true);
    expect(matchOperator.getKey()).toEqual(testKey);
    expect(matchOperator.getValue()).toEqual(testValue);

    const regExpOperator = operatorBuilder.createRegExpOperator(
      testKey,
      testRegExp
    );
    expect(regExpOperator instanceof RegularExpresionDatabaseOperator).toEqual(
      true
    );
    expect(regExpOperator.getKey()).toEqual(testKey);
    expect(regExpOperator.getValue()).toEqual(testRegExp);
  });
});
