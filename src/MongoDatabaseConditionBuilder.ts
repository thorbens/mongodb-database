import {
  AbstractDatabaseConditionBuilder,
  AtomDatabaseOperator,
  DatabaseOperator,
  KeyModel,
  SearchDatabaseOperator,
} from "@thorbens/database";
import {
  AndDatabaseOperator,
  EqualDatabaseOperator,
  ExistsDatabaseOperator,
  FullTextDatabaseOperator,
  GreaterThanDatabaseOperator,
  GreaterThanOrEqualDatabaseOperator,
  InDatabaseOperator,
  LessThanDatabaseOperator,
  LessThanOrEqualDatabaseOperator,
  MatchSomeDatabaseOperator,
  MongoFullTextSearchOptions,
  NotDatabaseOperator,
  NotEqualDatabaseOperator,
  OrDatabaseOperator,
  RegularExpresionDatabaseOperator,
} from "./Operators";

/**
 * MongoDatabase Implementation of DatabaseConditionBuilder factory.
 */
export class MongoDatabaseConditionBuilder<
  TModel = {}
> extends AbstractDatabaseConditionBuilder<TModel> {
  public createEqualOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ) {
    return new EqualDatabaseOperator(key, value);
  }

  public createNotEqualOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ) {
    return new NotEqualDatabaseOperator(key, value);
  }

  public createNotOperator(value: DatabaseOperator<TModel>[]) {
    return new NotDatabaseOperator<TModel>(value);
  }

  public createMatchSomeOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ) {
    return new MatchSomeDatabaseOperator(key, value);
  }

  public createInOperator<TValue extends unknown[] = []>(
    key: KeyModel<TModel>,
    value: TValue
  ) {
    return new InDatabaseOperator(key, value);
  }

  public createRegExpOperator<TValue extends RegExp = RegExp>(
    key: KeyModel<TModel>,
    regExp: TValue
  ) {
    return new RegularExpresionDatabaseOperator(key, regExp);
  }

  public createGreaterThanOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ) {
    return new GreaterThanDatabaseOperator(key, value);
  }

  public createLessThanOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ) {
    return new LessThanDatabaseOperator(key, value);
  }

  public createExistsOperator<
    TValue extends DatabaseOperator[] = DatabaseOperator[]
  >(key: KeyModel<TModel>, value: TValue) {
    return new ExistsDatabaseOperator(key, value);
  }

  public createAndOperator(value: DatabaseOperator<TModel>[]) {
    return new AndDatabaseOperator(value);
  }

  public createOrOperator(value: DatabaseOperator<TModel>[]) {
    return new OrDatabaseOperator(value);
  }

  public createFullTextOperator<TValue extends string = string>(
    searchTerm: TValue,
    options?: MongoFullTextSearchOptions
  ): SearchDatabaseOperator<TValue> {
    return new FullTextDatabaseOperator(searchTerm, options || {});
  }

  public createGreaterThanOrEqualOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ) {
    return new GreaterThanOrEqualDatabaseOperator(key, value);
  }

  public createLessThanOrEqualOperator<TValue = unknown>(
    key: KeyModel<TModel>,
    value: TValue
  ) {
    return new LessThanOrEqualDatabaseOperator(key, value);
  }

  public createJoinOperator(
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    name: KeyModel<TModel>,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    condition: DatabaseOperator
  ): AtomDatabaseOperator<TModel> {
    throw new Error("unsupported");
  }
}
