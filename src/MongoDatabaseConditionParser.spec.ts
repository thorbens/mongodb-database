import {
  AbstractLogicalDatabaseOperator,
  CommonDatabaseOperatorObject,
  CommonDatabaseOperatorType,
  LogicalDatabaseOperator,
} from "@thorbens/database";
import { MongoDatabaseConditionParser } from "./MongoDatabaseConditionParser";
import { AndDatabaseOperator, OrDatabaseOperator } from "./Operators";

describe("MongoDatabaseConditionParser", () => {
  it("should parse an and condition correctly", async () => {
    const conditionParser = new MongoDatabaseConditionParser();
    const value = `foo`;
    const type = CommonDatabaseOperatorType.AND;
    const condition: CommonDatabaseOperatorObject = {
      type,
      value,
    };

    const parsedCondition = conditionParser.parse(condition);
    if (!parsedCondition) {
      throw new Error(`parsing should not return null`);
    }

    expect(parsedCondition.getValue()).toEqual(value);
    expect(parsedCondition.getType()).toEqual(type);
    expect(parsedCondition instanceof AbstractLogicalDatabaseOperator).toEqual(
      true
    );
  });

  it("should parse nested condition correctly", async () => {
    const conditionParser = new MongoDatabaseConditionParser();
    const condition: CommonDatabaseOperatorObject = {
      type: CommonDatabaseOperatorType.AND,
      value: [
        {
          type: "or",
          value: [
            {
              key: "type",
              type: CommonDatabaseOperatorType.EQUAL,
              value: "MOVIE",
            },
          ],
        },
      ],
    };

    const parsedCondition = conditionParser.parse(
      condition
    ) as LogicalDatabaseOperator<unknown>;
    if (!parsedCondition) {
      throw new Error(`parsing should not return null`);
    }

    expect(parsedCondition instanceof AndDatabaseOperator).toEqual(true);
    expect(parsedCondition.getType()).toEqual(CommonDatabaseOperatorType.AND);
    // get the value of the parsed and condition
    const andConditionValue = parsedCondition.getValue();
    // there should be one value
    expect(andConditionValue.length).toEqual(1);
    // get the or condition
    const parsedOrCondition = andConditionValue[0];
    expect(parsedOrCondition instanceof OrDatabaseOperator).toEqual(true);
    expect(parsedOrCondition.getType()).toEqual(CommonDatabaseOperatorType.OR);
  });
});
