import {
  AbstractDatabaseConditionParser,
  DatabaseConditionBuilder,
} from "@thorbens/database";
import { MongoDatabaseConditionBuilder } from "./MongoDatabaseConditionBuilder";

export class MongoDatabaseConditionParser<
  TModel = {}
> extends AbstractDatabaseConditionParser<TModel> {
  private readonly conditionBuilder: MongoDatabaseConditionBuilder<TModel>;
  private readonly dateRegExp = /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/;

  public constructor(conditionBuilder?: MongoDatabaseConditionBuilder<TModel>) {
    super();
    this.conditionBuilder =
      conditionBuilder || new MongoDatabaseConditionBuilder();
  }

  protected isDate(value: string): boolean {
    return this.dateRegExp.test(value);
  }

  protected getConditionBuilder(): DatabaseConditionBuilder<TModel> {
    return this.conditionBuilder;
  }
}
