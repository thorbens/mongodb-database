import {
  AbstractDatabaseResult,
  DatabaseAggregationMap,
} from "@thorbens/database";
import { ReadOnlyDatabaseQuery } from "@thorbens/database/src/ReadOnlyDatabaseQuery";

export class MongoDatabaseResult<
  TSource,
  TResult = TSource
> extends AbstractDatabaseResult<TSource, TResult> {
  private readonly totalResultCountCallback: () => Promise<number>;
  private readonly aggregationCallback: () => Promise<DatabaseAggregationMap>;
  private totalCount?: number;
  private aggregations?: DatabaseAggregationMap;

  public constructor(
    query: ReadOnlyDatabaseQuery<TSource, TResult>,
    results: TResult[],
    totalResultCountCallback: () => Promise<number>,
    aggregationCallback: () => Promise<DatabaseAggregationMap>
  ) {
    super(query, results);
    this.totalResultCountCallback = totalResultCountCallback;
    this.aggregationCallback = aggregationCallback;
  }

  public async getAggregations(): Promise<DatabaseAggregationMap> {
    if (!this.aggregations) {
      this.aggregations = await this.aggregationCallback();
    }

    return this.aggregations;
  }

  public async getTotalSize(): Promise<number> {
    if (this.totalCount !== undefined) {
      return this.totalCount;
    }
    this.totalCount = await this.totalResultCountCallback();

    return this.totalCount;
  }
}
