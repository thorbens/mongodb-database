import { DatabaseIDProvider } from "@thorbens/database";
import { MongoDatabase } from "./MongoDatabase";
import { IndexDefinition } from "./MongoRepository";

const database = new MongoDatabase({
  databaseName: process.env.MONGO_DB_DATABASE_NAME || "test",
  password: process.env.MONGO_DB_PASSWORD || "test",
  url: process.env.MONGO_DB_HOST || "mongodb://localhost:27017",
  username: process.env.MONGO_DB_USERNAME || "root",
});

// tslint:disable-next-line:max-classes-per-file
class TestDatabaseModel implements DatabaseIDProvider {
  public foo = "bar";
  public id = "";
}

// tslint:disable-next-line:max-classes-per-file
class MultipleFieldTestDatabaseModel implements DatabaseIDProvider {
  public foo = "bar";
  public bar = "foo";
  public id = "";
}

describe("MongoRepository", () => {
  beforeEach(async () => {
    // drop repository between each action
    await Promise.all([
      database.dropRepository(TestDatabaseModel),
      database.dropRepository(MultipleFieldTestDatabaseModel),
    ]);
  });

  afterAll(async () => {
    await database.disconnect();
  });

  it("should return the correct projection result", async () => {
    // define model an projection
    type ModelProjection = Pick<TestDatabaseModel, "foo">;

    const repository = await database.getRepository<TestDatabaseModel>(
      TestDatabaseModel
    );
    const model: TestDatabaseModel = {
      id: "1",
      foo: "bar",
    };
    await repository.insertOne(model);

    // create query
    const conditionBuilder = database.getConditionBuilder<TestDatabaseModel>();
    const query = repository.createQuery<ModelProjection>();
    query.addCondition(conditionBuilder.createEqualOperator("id", "1"));
    query.setFields(["foo"]);

    // perform query
    const result = await repository.findOneByQuery<ModelProjection>(query);

    expect(result.getFirstResult()).toEqual({
      foo: "bar",
    });
  });

  it("should perform a count correct", async () => {
    const repository = await database.getRepository(TestDatabaseModel);
    const query = repository.createQuery();
    // should be empty by default
    expect(await repository.count(query)).toEqual(0);

    await repository.insertOne({
      id: "1",
      foo: "bar",
    });
    // after inserting there should be one element
    expect(await repository.count(query)).toEqual(1);
  });

  it("should perform a total count correct", async () => {
    const repository = await database.getRepository(TestDatabaseModel);
    const query = repository.createQuery();
    // only retrieve on element
    query.setLimit(1);

    // actually insert 3 elements
    await repository.insert([
      {
        id: "1",
        foo: "one",
      },
      {
        id: "2",
        foo: "two",
      },
      {
        id: "3",
        foo: "three",
      },
    ]);
    // after inserting there should be one element
    const result = await repository.findByQuery(query);
    expect(result.getSize()).toEqual(1);
    expect(await result.getTotalSize()).toEqual(3);
  });

  it("should insert and lookup the inserted element", async () => {
    const repository = await database.getRepository(TestDatabaseModel);
    const model = {
      id: "1",
      foo: "bar",
    };
    expect(await repository.findById(model.id)).toEqual(null);

    await repository.insertOne(model);
    expect(await repository.findById(model.id)).toEqual(model);
  });

  it("should insert and delete the inserted element", async () => {
    const repository = await database.getRepository(TestDatabaseModel);
    const model = {
      id: "1",
      foo: "bar",
    };
    await repository.insertOne(model);
    expect(await repository.findById(model.id)).not.toEqual(null);
    // delete element
    await repository.deleteById(model.id);
    expect(await repository.findById(model.id)).toEqual(null);
  });

  it("should return the correct name of the repository", async () => {
    const repository = await database.getRepository(TestDatabaseModel);
    expect(await repository.getName()).toEqual("TestDatabaseModel");
  });

  it("should create the index correctly", async () => {
    const repository = await database.getRepository<TestDatabaseModel>(
      TestDatabaseModel
    );
    const field = "foo";
    expect(await repository.hasIndex(field)).toEqual(false);
    await repository.createIndex(field);
    expect(await repository.hasIndex(field)).toEqual(true);
    await repository.deleteIndex(field);
    expect(await repository.hasIndex(field)).toEqual(false);
  });

  it("should create a text index correctly", async () => {
    const repository = await database.getRepository<TestDatabaseModel>(
      TestDatabaseModel
    );
    const field = "foo";
    expect(await repository.hasIndex(field)).toEqual(false);
    await repository.createIndex(field, "text");
    expect(await repository.hasIndex(field)).toEqual(true);
    await repository.deleteIndex(field);
    expect(await repository.hasIndex(field)).toEqual(false);
  });

  it("should create the index and multiple fields correctly", async () => {
    const repository = await database.getRepository<MultipleFieldTestDatabaseModel>(
      MultipleFieldTestDatabaseModel
    );
    const fields: IndexDefinition<keyof MultipleFieldTestDatabaseModel>[] = [
      { property: "foo" },
      { property: "bar" },
    ];
    for (const field of fields) {
      expect(await repository.hasIndex(field.property)).toEqual(false);
    }
    await repository.createIndexes(fields);
    for (const field of fields) {
      expect(await repository.hasIndex(field.property)).toEqual(true);
    }
  });

  it("should allow a count on an empty repository", async () => {
    const repository = await database.getRepository<TestDatabaseModel>(
      TestDatabaseModel
    );
    expect(await repository.count()).toEqual(0);
  });

  it("should aggregate the result correctly", async () => {
    const models: MultipleFieldTestDatabaseModel[] = [
      {
        id: "1",
        foo: "bar",
        bar: "first",
      },
      {
        id: "2",
        foo: "bar",
        bar: "second",
      },
      {
        id: "3",
        foo: "test",
        bar: "third",
      },
    ];
    const repository = await database.getRepository<MultipleFieldTestDatabaseModel>(
      MultipleFieldTestDatabaseModel
    );
    await repository.insert(models);

    const query = database.createQuery<MultipleFieldTestDatabaseModel>();
    query.setAggregationFields({
      foo: "foo",
      fooBar: "bar",
    });

    const aggregations = await repository.aggregate(query);

    expect(aggregations.foo).toBeDefined();
    expect(aggregations.foo.bar).toEqual(2);
    expect(aggregations.foo.test).toEqual(1);
    expect(aggregations.fooBar).toBeDefined();
    expect(aggregations.fooBar.first).toEqual(1);
    expect(aggregations.fooBar.second).toEqual(1);
    expect(aggregations.fooBar.third).toEqual(1);
  });
});
