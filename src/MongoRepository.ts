import {
  CommonDatabaseQuery,
  DatabaseAggregationMap,
  DatabaseIDProvider,
  DatabaseModel,
  DatabaseOrderByDirection,
  DatabaseQuery,
  DatabaseResult,
  ReadOnlyDatabaseQuery,
  Repository,
} from "@thorbens/database";
import { Collection, Filter, FindOptions } from "mongodb";
import { MongoDatabaseCollectionOptions } from "./mapping";
import { MongoDatabaseResult } from "./MongoDatabaseResult";
import {
  createAggregationFacet,
  createAggregationMapFromResult,
  enhanceModelWithId,
} from "./helper";

export type IndexType = 1 | -1 | "text";

export interface IndexDefinition<TProperty = string> {
  property: TProperty | string;
  type?: IndexType;
}

/**
 * Repository for crud operation for the given collection.
 */
export class MongoRepository<T extends DatabaseModel> implements Repository<T> {
  // TODO: generic typings requires to use Filter<T> typing
  private readonly collection: Collection<any>;
  private readonly options: MongoDatabaseCollectionOptions | undefined;

  public constructor(
    collection: Collection<T>,
    options?: MongoDatabaseCollectionOptions
  ) {
    this.collection = collection;
    this.options = options;
  }

  public async updateMappings(): Promise<void> {
    if (!this.options) {
      return;
    }
    if (this.options.properties) {
      const indexDefinitions: IndexDefinition<keyof T>[] = Object.entries(
        this.options.properties
      )
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        .filter(([_, property]) => property.createIndex)
        .map(([property, entry]) => ({
          property,
          type: entry.indexType,
        }));

      if (indexDefinitions.length > 0) {
        await this.createIndexes(indexDefinitions);
      }
    }
  }

  public async drop(): Promise<void> {
    await this.collection.drop();
  }

  public getName(): Promise<string> {
    return Promise.resolve(this.collection.collectionName);
  }

  public async insertOne(element: Readonly<T>): Promise<void> {
    await this.collection.insertOne(enhanceModelWithId(element));
  }

  public async insert(elements: ReadonlyArray<T>): Promise<void> {
    const mergedElements = elements.map(enhanceModelWithId);

    await this.collection.insertMany(mergedElements);
  }

  public async insertOrReplaceOne(element: Readonly<T>): Promise<void> {
    await this.collection.replaceOne(
      {
        id: element.id,
      },
      element,
      {
        upsert: true,
      }
    );
  }

  public async replaceOne(element: Readonly<T>): Promise<void> {
    await this.collection.replaceOne(
      {
        id: element.id,
      },
      element
    );
  }

  public async updateOneField(
    id: string,
    field: keyof T,
    data: T[keyof T]
  ): Promise<void> {
    await this.collection.updateOne(
      {
        id,
      },
      {
        $set: {
          [field]: data,
        },
      }
    );
  }

  public async updateOne(data: Partial<T> & DatabaseIDProvider): Promise<void> {
    await this.collection.updateOne(
      {
        id: data.id,
      },
      {
        $set: data,
      }
    );
  }

  public async update(
    query: ReadOnlyDatabaseQuery<T>,
    data: Partial<T>
  ): Promise<void> {
    // create filters
    const filters = query.toObject();
    // perform update
    await this.collection.updateMany(filters, {
      $set: data,
    });
  }

  public async findById(id: string): Promise<T | null> {
    return this.collection.findOne(
      {
        id,
      },
      {
        projection: {
          _id: false,
        },
      }
    );
  }

  public async findByQuery<TResult = T>(
    query: ReadOnlyDatabaseQuery<T, TResult>
  ): Promise<DatabaseResult<T, TResult>> {
    const filters = query.toObject();
    const options = this.createOptionsFromQuery(query);
    const cursor = this.collection.find(filters, options);
    // adjust sizing to cursor, 0 is equivalent to setting no limit.
    // @see https://docs.mongodb.com/manual/reference/method/cursor.limit/#zero-value
    cursor.limit(query.getLimit());
    cursor.skip(query.getOffset());
    // order elements
    const orderBy = query.getOrderBy();
    if (orderBy) {
      cursor.sort(
        orderBy,
        query.getOrderByDirection() === DatabaseOrderByDirection.ASC ? 1 : -1
      );
    }
    const results = await cursor.toArray();

    return this.createDatabaseResult(
      query,
      results,
      () =>
        cursor.count({
          skip: 0,
          limit: 0,
        }),
      () => this.aggregate(query)
    );
  }

  public async findOneByQuery<TResult = T>(
    query: ReadOnlyDatabaseQuery<T, TResult>
  ): Promise<DatabaseResult<T, TResult>> {
    const filters = query.toObject();
    const options = this.createOptionsFromQuery(query);
    const result = await this.collection.findOne(filters, options);

    return this.createDatabaseResult(
      query,
      result ? [result] : [],
      () => Promise.resolve(1),
      () => Promise.resolve({})
    );
  }

  public createQuery<TResult = T>(): DatabaseQuery<T, TResult> {
    return new CommonDatabaseQuery<T, TResult>();
  }

  public async deleteById(id: string): Promise<void> {
    await this.collection.deleteOne({
      id,
    });
  }

  public async deleteByQuery(query: ReadOnlyDatabaseQuery<T>): Promise<void> {
    const filters = query.toObject();

    await this.collection.deleteMany(filters);
  }

  public count(query?: ReadOnlyDatabaseQuery<T>): Promise<number> {
    const filters = query?.toObject() as Filter<T>;

    return this.collection.countDocuments(filters);
  }

  /**
   * Creates an index on the given fields of this collections.
   *
   * @param indexDefinitions The index definitions to create.
   */
  public async createIndexes(
    indexDefinitions: ReadonlyArray<IndexDefinition<keyof T>>
  ): Promise<void> {
    await Promise.all(
      indexDefinitions.map((definition) =>
        this.createIndex(definition.property, definition.type)
      )
    );
  }

  /**
   * Creates an index on the given fields of this collections.
   *
   * @param field The field to create an index on.
   * @param type The type of the index. By default, a descending index is created. See <a href="https://docs.mongodb.com/manual/reference/method/db.collection.createIndex/#mongodb-method-db.collection.createIndex">collection.createIndex</a> for more information.
   */
  public async createIndex(
    field: keyof T | string,
    type: IndexType = 1
  ): Promise<void> {
    await this.collection.createIndex(
      {
        [field]: type,
      },
      {
        name: `${field}_index`,
      }
    );
  }

  /**
   * Checks if an index exists on the given field.
   *
   * @param field The field to check an index for.
   */
  public hasIndex(field: keyof T | string): Promise<boolean> {
    return this.collection.indexExists(`${field}_index`);
  }

  /**
   * Deletes an index on the given field.
   *
   * @param field The field to delete the index for.
   */
  public async deleteIndex(field: keyof T | string): Promise<void> {
    await this.collection.dropIndex(`${field}_index`);
  }

  public getCollection() {
    return this.collection;
  }

  /**
   * Performs an aggregation based on the provides query.
   * The {@link DatabaseQuery#setAggregationFields} must be set,
   * otherwise an empty object is returned.
   *
   * @param query The query to use.
   */
  public async aggregate<TQuery, TResult>(
    query: ReadOnlyDatabaseQuery<TQuery, TResult>
  ): Promise<DatabaseAggregationMap> {
    const aggregationFields = query.getAggregationFields();
    if (aggregationFields.length === 0) {
      return {};
    }

    const facets = createAggregationFacet(aggregationFields);
    const filters = query.toObject();
    const aggregationCursor = await this.collection.aggregate([
      {
        $match: filters,
      },
      {
        $facet: facets,
      },
    ]);

    const aggregationResults = await aggregationCursor.toArray();

    return createAggregationMapFromResult(aggregationResults);
  }

  /**
   * Creates find options from the given query.
   *
   * @param query The query to create options form.
   */
  private createOptionsFromQuery<TResult = T>(
    query: ReadOnlyDatabaseQuery<T, TResult>
  ): FindOptions<T> {
    const options: FindOptions<T> = {
      projection: {
        _id: false,
      },
    };
    if (query.getFields().length) {
      options.projection = query.getFields().reduce((collector, field) => {
        return {
          ...collector,
          [field]: true,
        };
      }, options.projection);
    }

    return options;
  }

  /**
   * Creates a database result from the given query and the given results.
   *
   * @param query The query to use.
   * @param results The results to use.
   * @param totalResultCountCallback A function to get the total count for the given query.
   * @param aggregationsCallback A function to get a map of requested aggregations.
   */
  private createDatabaseResult<TSource, TResult = TSource>(
    query: ReadOnlyDatabaseQuery<TSource, TResult>,
    results: TResult[],
    totalResultCountCallback: () => Promise<number>,
    aggregationsCallback: () => Promise<DatabaseAggregationMap>
  ): MongoDatabaseResult<TSource, TResult> {
    return new MongoDatabaseResult<TSource, TResult>(
      query,
      results,
      totalResultCountCallback,
      aggregationsCallback
    );
  }
}
