import { CommonDatabaseOperatorType } from "@thorbens/database";
import { AndDatabaseOperator } from "./AndDatabaseOperator";
import { EqualDatabaseOperator } from "./EqualDatabaseOperator";

describe("AndDatabaseOperator", () => {
  it("should create the operator correctly", async () => {
    const values = [new EqualDatabaseOperator(`foo`, `bar`)];
    const condition = new AndDatabaseOperator(values);

    expect(condition.getValue()).toEqual(values);
    expect(condition.toObject()).toEqual({
      $and: values.map((value) => value.toObject()),
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.AND);
  });
});
