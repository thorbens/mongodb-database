import {
  AbstractLogicalDatabaseOperator,
  CommonDatabaseOperatorType,
} from "@thorbens/database";

/**
 * @see {@link https://docs.mongodb.com/manual/reference/operator/query/and/}
 */
export class AndDatabaseOperator<
  TModel
> extends AbstractLogicalDatabaseOperator<TModel> {
  public readonly type = CommonDatabaseOperatorType.AND;

  public toObject() {
    return {
      $and: this.getConditionValue(),
    };
  }

  public getType(): CommonDatabaseOperatorType {
    return this.type;
  }
}
