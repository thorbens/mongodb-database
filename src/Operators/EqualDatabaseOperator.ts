import {
  AbstractAtomDatabaseOperator,
  CommonDatabaseOperatorType,
} from "@thorbens/database";

/**
 * @see {@link https://docs.mongodb.com/manual/reference/operator/query/eq/}
 */
export class EqualDatabaseOperator<
  TModel,
  TValue = unknown
> extends AbstractAtomDatabaseOperator<TModel, TValue> {
  public readonly type = CommonDatabaseOperatorType.EQUAL;

  public toObject() {
    return {
      [this.getKey()]: this.getConditionValue(),
    };
  }

  public getType(): CommonDatabaseOperatorType {
    return this.type;
  }
}
