import {
  AbstractAtomDatabaseOperator,
  CommonDatabaseOperatorType,
} from "@thorbens/database";

/**
 * @see {@link https://docs.mongodb.com/manual/reference/operator/query/exists/}
 */
export class ExistsDatabaseOperator<
  TModel,
  TValue = unknown
> extends AbstractAtomDatabaseOperator<TModel, TValue> {
  public readonly type = CommonDatabaseOperatorType.EXISTS;

  public toObject() {
    return {
      $in: this.getConditionValue(),
    };
  }

  public getType(): CommonDatabaseOperatorType {
    return this.type;
  }
}
