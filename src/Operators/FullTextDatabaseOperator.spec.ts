import { CommonDatabaseOperatorType, SearchOptions } from "@thorbens/database";
import {
  FullTextDatabaseOperator,
  MongoFullTextSearchOptions,
} from "./FullTextDatabaseOperator";

describe("EqualDatabaseOperator", () => {
  it("should create the operator correctly", async () => {
    const searchTerm = "bar";
    const options: SearchOptions = {};
    const condition = new FullTextDatabaseOperator(searchTerm, options);

    expect(condition.getValue()).toEqual(searchTerm);
    expect(condition.getOptions()).toEqual(options);

    expect(condition.toObject()).toEqual({
      $text: {
        $search: searchTerm,
      },
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.FULL_TEXT);
  });

  it("should create the operator correctly with the given options", async () => {
    const searchTerm = "bar";
    const options: MongoFullTextSearchOptions = {
      $language: `de`,
      $caseSensitive: true,
      $diacriticSensitive: true,
    };
    const condition = new FullTextDatabaseOperator(searchTerm, options);

    expect(condition.getValue()).toEqual(searchTerm);
    expect(condition.getOptions()).toEqual(options);

    expect(condition.toObject()).toEqual({
      $text: {
        $search: searchTerm,
        $language: `de`,
        $caseSensitive: true,
        $diacriticSensitive: true,
      },
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.FULL_TEXT);
  });
});
