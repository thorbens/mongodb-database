import { CommonDatabaseOperatorType, SearchOptions } from "@thorbens/database";
import { AbstractSearchDatabaseOperator } from "@thorbens/database/dist/AbstractSearchDatabaseOperator";

export interface MongoFullTextSearchOptions extends SearchOptions {
  $language?: string;
  $caseSensitive?: boolean;
  $diacriticSensitive?: boolean;
}

/**
 * @see {@link https://docs.mongodb.com/manual/reference/operator/query/text/#op._S_text}
 */
export class FullTextDatabaseOperator<
  TValue extends string = string
> extends AbstractSearchDatabaseOperator<TValue> {
  public readonly type = CommonDatabaseOperatorType.FULL_TEXT;

  public constructor(searchTerm: TValue, options: MongoFullTextSearchOptions) {
    super(searchTerm, options);
  }

  public toObject() {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { fields: _, ...rest } = this.getOptions();

    return {
      $text: {
        $search: this.getValue(),
        ...rest,
      },
    };
  }

  public getType(): CommonDatabaseOperatorType {
    return this.type;
  }

  public getOptions(): MongoFullTextSearchOptions {
    return super.getOptions();
  }
}
