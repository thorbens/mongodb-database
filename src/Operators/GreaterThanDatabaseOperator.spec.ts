import { CommonDatabaseOperatorType } from "@thorbens/database";
import { GreaterThanDatabaseOperator } from "./GreaterThanDatabaseOperator";

describe("GreaterThanDatabaseOperator", () => {
  it("should create the operator correctly", async () => {
    const value = "bar";
    const key = "foo";
    const condition = new GreaterThanDatabaseOperator(key, value);

    expect(condition.getKey()).toEqual(key);
    expect(condition.getValue()).toEqual(value);
    expect(condition.toObject()).toEqual({
      [key]: {
        $gt: value,
      },
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.GT);
  });
});
