import { CommonDatabaseOperatorType } from "@thorbens/database";
import { GreaterThanOrEqualDatabaseOperator } from "./GreaterThanOrEqualDatabaseOperator";

describe("GreaterThanOrEqualDatabaseOperator", () => {
  it("should create the operator correctly", async () => {
    const value = "bar";
    const key = "foo";
    const condition = new GreaterThanOrEqualDatabaseOperator(key, value);

    expect(condition.getKey()).toEqual(key);
    expect(condition.getValue()).toEqual(value);
    expect(condition.toObject()).toEqual({
      [key]: {
        $gte: value,
      },
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.GTE);
  });
});
