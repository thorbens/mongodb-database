import {
  AbstractAtomDatabaseOperator,
  CommonDatabaseOperatorType,
} from "@thorbens/database";

/**
 * @see {@link https://docs.mongodb.com/manual/reference/operator/query/gte/}
 */
export class GreaterThanOrEqualDatabaseOperator<
  TModel,
  TValue = unknown
> extends AbstractAtomDatabaseOperator<TModel, TValue> {
  public readonly type = CommonDatabaseOperatorType.GTE;

  public toObject() {
    return {
      [this.getKey()]: {
        $gte: this.getConditionValue(),
      },
    };
  }

  public getType(): CommonDatabaseOperatorType {
    return this.type;
  }
}
