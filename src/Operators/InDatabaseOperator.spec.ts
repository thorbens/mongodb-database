import { CommonDatabaseOperatorType } from "@thorbens/database";
import { InDatabaseOperator } from "./InDatabaseOperator";

describe("AndDatabaseOperator", () => {
  it("should create the operator correctly", async () => {
    const values = [`foo`, `bar`];
    const condition = new InDatabaseOperator("id", values);

    expect(condition.getValue()).toEqual(values);
    expect(condition.toObject()).toEqual({
      id: {
        $in: values,
      },
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.IN);
  });
});
