import { CommonDatabaseOperatorType } from "@thorbens/database";
import { LessThanDatabaseOperator } from "./LessThanDatabaseOperator";

describe("LessThanDatabaseOperator", () => {
  it("should create the operator correctly", async () => {
    const value = "bar";
    const key = "foo";
    const condition = new LessThanDatabaseOperator(key, value);

    expect(condition.getKey()).toEqual(key);
    expect(condition.getValue()).toEqual(value);
    expect(condition.toObject()).toEqual({
      [key]: {
        $lt: value,
      },
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.LT);
  });
});
