import {
  AbstractAtomDatabaseOperator,
  CommonDatabaseOperatorType,
} from "@thorbens/database";

/**
 * @see {@link https://docs.mongodb.com/manual/reference/operator/query/lt/}
 */
export class LessThanDatabaseOperator<
  TModel,
  TValue = unknown
> extends AbstractAtomDatabaseOperator<TModel, TValue> {
  public readonly type = CommonDatabaseOperatorType.LT;

  public toObject() {
    return {
      [this.getKey()]: {
        $lt: this.getConditionValue(),
      },
    };
  }

  public getType(): CommonDatabaseOperatorType {
    return this.type;
  }
}
