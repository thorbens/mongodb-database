import { CommonDatabaseOperatorType } from "@thorbens/database";
import { LessThanOrEqualDatabaseOperator } from "./LessThanOrEqualDatabaseOperator";

describe("LessThanOrEqualDatabaseOperator", () => {
  it("should create the operator correctly", async () => {
    const value = "bar";
    const key = "foo";
    const condition = new LessThanOrEqualDatabaseOperator(key, value);

    expect(condition.getKey()).toEqual(key);
    expect(condition.getValue()).toEqual(value);
    expect(condition.toObject()).toEqual({
      [key]: {
        $lte: value,
      },
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.LTE);
  });
});
