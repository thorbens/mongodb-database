import {
  AbstractAtomDatabaseOperator,
  CommonDatabaseOperatorType,
} from "@thorbens/database";

/**
 * @see {@link https://docs.mongodb.com/manual/reference/operator/query/lte/}
 */
export class LessThanOrEqualDatabaseOperator<
  TModel,
  TValue = unknown
> extends AbstractAtomDatabaseOperator<TModel, TValue> {
  public readonly type = CommonDatabaseOperatorType.LTE;

  public toObject() {
    return {
      [this.getKey()]: {
        $lte: this.getConditionValue(),
      },
    };
  }

  public getType(): CommonDatabaseOperatorType {
    return this.type;
  }
}
