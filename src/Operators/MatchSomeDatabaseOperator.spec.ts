import { CommonDatabaseOperatorType } from "@thorbens/database";
import { MatchSomeDatabaseOperator } from "./MatchSomeDatabaseOperator";

describe("MatchSomeDatabaseOperator", () => {
  it("should create the operator correctly", async () => {
    const value = "bar";
    const key = "foo";
    const condition = new MatchSomeDatabaseOperator(key, value);

    expect(condition.getKey()).toEqual(key);
    expect(condition.getValue()).toEqual(value);
    expect(condition.toObject()).toEqual({
      [key]: {
        $elemMatch: value,
      },
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.MATCH_SOME);
  });
});
