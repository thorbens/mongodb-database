import { CommonDatabaseOperatorType } from "@thorbens/database";
import { EqualDatabaseOperator } from "./EqualDatabaseOperator";
import { NotDatabaseOperator } from "./NotDatabaseOperator";

describe("NotDatabaseOperator", () => {
  it("should create the operator correctly", async () => {
    const value = "bar";
    const key = "foo";
    const values = [new EqualDatabaseOperator(key, value)];
    const condition = new NotDatabaseOperator(values);

    expect(condition.getValue()).toEqual(values);

    expect(condition.toObject()).toEqual({
      $not: {
        [key]: value,
      },
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.NOT);
  });
});
