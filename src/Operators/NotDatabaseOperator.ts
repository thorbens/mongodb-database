import {
  AbstractLogicalDatabaseOperator,
  CommonDatabaseOperatorType,
} from "@thorbens/database";

/**
 * @see {@link https://docs.mongodb.com/manual/reference/operator/query/not/}
 */
export class NotDatabaseOperator<
  TModel
> extends AbstractLogicalDatabaseOperator<TModel> {
  public readonly type = CommonDatabaseOperatorType.NOT;

  public toObject() {
    let conditionObject;
    const conditions = this.getConditionValue();
    if (Array.isArray(conditions)) {
      conditionObject = conditions.reduce((object, conditionValue) => {
        return {
          ...object,
          ...conditionValue,
        };
      }, {});
    } else {
      conditionObject = {
        ...conditions,
      };
    }

    return {
      $not: conditionObject,
    };
  }

  public getType(): CommonDatabaseOperatorType {
    return this.type;
  }
}
