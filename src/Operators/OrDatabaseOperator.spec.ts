import { CommonDatabaseOperatorType } from "@thorbens/database";
import { EqualDatabaseOperator } from "./EqualDatabaseOperator";
import { OrDatabaseOperator } from "./OrDatabaseOperator";

describe("OrDatabaseOperator", () => {
  it("should create the operator correctly", async () => {
    const values = [new EqualDatabaseOperator(`foo`, `bar`)];
    const condition = new OrDatabaseOperator(values);

    expect(condition.getValue()).toEqual(values);
    const toObject = condition.toObject();
    expect(toObject).toEqual({
      $or: values.map((value) => value.toObject()),
    });
    expect(condition.getType()).toEqual(CommonDatabaseOperatorType.OR);
  });
});
