import {
  AbstractLogicalDatabaseOperator,
  CommonDatabaseOperatorType,
} from "@thorbens/database";

/**
 * @see {@link https://docs.mongodb.com/manual/reference/operator/query/or/}
 */
export class OrDatabaseOperator<
  TModel
> extends AbstractLogicalDatabaseOperator<TModel> {
  public readonly type = CommonDatabaseOperatorType.OR;

  public toObject() {
    return {
      $or: this.getConditionValue(),
    };
  }

  public getType(): CommonDatabaseOperatorType {
    return this.type;
  }
}
