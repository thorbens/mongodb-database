import {
  AbstractAtomDatabaseOperator,
  CommonDatabaseOperatorType,
} from "@thorbens/database";

/**
 * @see {@link https://docs.mongodb.com/manual/reference/operator/query/regex/#op._S_regex}
 */
export class RegularExpresionDatabaseOperator<
  TModel,
  TValue extends RegExp = RegExp
> extends AbstractAtomDatabaseOperator<TModel, TValue> {
  public readonly type = CommonDatabaseOperatorType.REGEXP;

  public toObject() {
    return {
      [this.getKey()]: {
        $regex: this.getValue(),
      },
    };
  }

  public getType(): CommonDatabaseOperatorType {
    return this.type;
  }
}
