import { createAggregationFacet } from "./createAggregationFacet";

describe("createAggregationFacet", () => {
  it("should create the aggregation facets correctly", () => {
    const fields = {
      foo: "foo",
      bar: "bar",
    };

    const facets = createAggregationFacet(fields);

    expect(facets).toEqual({
      foo: [
        {
          $group: {
            _id: "$foo",
            hits: {
              $sum: 1,
            },
          },
        },
      ],
      bar: [
        {
          $group: {
            _id: "$bar",
            hits: {
              $sum: 1,
            },
          },
        },
      ],
    });
  });
});
