/**
 * Creates a simple [$facet](https://docs.mongodb.com/manual/reference/operator/aggregation/facet/) which performs a
 * [$group](https://docs.mongodb.com/manual/reference/operator/aggregation/group/) on
 * each of the given aggregation fields with the [$sum](https://docs.mongodb.com/manual/reference/operator/aggregation/sum/#mongodb-group-grp.-sum)
 * included in a `hits` field.
 *
 * @param aggregationFields
 */
export function createAggregationFacet(
  aggregationFields: Record<string, string | number | Symbol>
) {
  return Object.entries(aggregationFields).reduce(
    (collector, [name, field]) => ({
      ...collector,
      [name]: [
        {
          $group: {
            _id: `$${field}`,
            hits: {
              $sum: 1,
            },
          },
        },
      ],
    }),
    {}
  );
}
