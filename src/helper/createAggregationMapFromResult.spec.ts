import { createAggregationMapFromResult } from "./createAggregationMapFromResult";

describe("createAggregationMapFromResult", () => {
  it("should create the result correctly", () => {
    const result = [
      {
        foo: [
          {
            _id: "bar",
            hits: 2,
          },
          {
            _id: "test",
            hits: 1,
          },
        ],
        bar: [
          {
            _id: "first",
            hits: 1,
          },
          {
            _id: "second",
            hits: 1,
          },
          {
            _id: "third",
            hits: 1,
          },
        ],
      },
    ];
    const aggregationResult = createAggregationMapFromResult(result);

    expect(aggregationResult.foo).toBeDefined();
    expect(aggregationResult.foo.bar).toEqual(2);
    expect(aggregationResult.foo.test).toEqual(1);
    expect(aggregationResult.bar).toBeDefined();
    expect(aggregationResult.bar.first).toEqual(1);
    expect(aggregationResult.bar.second).toEqual(1);
    expect(aggregationResult.bar.third).toEqual(1);
  });
});
