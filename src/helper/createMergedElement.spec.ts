import { enhanceModelWithId } from "./enhanceModelWithId";

describe("enhanceModelWithId", () => {
  it("should enhance the model with an _id correctly", () => {
    const model = {
      id: "foo",
    };
    const enhancedModel = enhanceModelWithId(model);

    expect(enhancedModel).toEqual({
      ...model,
      _id: "foo",
    });
  });
});
