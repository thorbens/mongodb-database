import { MongoDatabaseIDProvider } from "../Model";
import { DatabaseIDProvider } from "@thorbens/database";

/**
 * Maps the id of the given element to the mongo database id.
 *
 * @param element The element to merge.
 */
export function enhanceModelWithId<T extends DatabaseIDProvider>(
  element: Readonly<T>
): T & MongoDatabaseIDProvider {
  return Object.assign({}, element, {
    _id: element.id,
  });
}
