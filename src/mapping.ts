import { IndexType } from "./MongoRepository";

export interface MongoDatabaseFieldOptions {
  createIndex?: boolean;
  indexType?: IndexType;
}

export interface MongoDatabaseCollectionOptions {
  collectionName?: string;
  properties?: Record<string, MongoDatabaseFieldOptions>;
}

export interface MongoDatabaseOptionsProvider {
  /**
   * Returns the mapping for the index.
   */
  getCollectionOptions(): MongoDatabaseCollectionOptions | undefined;
}

export function isMongoDatabaseOptionsProvider(
  input: unknown
): input is MongoDatabaseOptionsProvider {
  return (
    typeof input === "object" &&
    input != null &&
    "getCollectionOptions" in input
  );
}
